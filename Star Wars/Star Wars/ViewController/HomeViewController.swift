//
//  HomeViewController.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {
}

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var answer2loading: UIActivityIndicatorView!
    @IBOutlet weak var answer3loading: UIActivityIndicatorView!
    
    @IBOutlet weak var leadingConstantAnimationBar: NSLayoutConstraint!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var leftStarButton: UIButton!
    @IBOutlet weak var rightStartButton: UIButton!
    
    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer4Label: UILabel!
    
    var allFilms = [Film]()
    
    var filmsUrl = Services.filmsUrl
    var starWarAnimating = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // get answers button actions
    
    @IBAction func getQuestionAnswersAction(_ sender: Any) {
        if(actionButton.isSelected) {
            starWarAnimating = true
            startStarWarAnimation(value: 220)
            loading.startAnimating()
            Services.getAllFilms(url: filmsUrl) { (error, allFilms, nextPage) in
                self.loading.stopAnimating()
                if(error == nil) {
                    if(nextPage == nil || nextPage == "") {
                        self.stopStarWarAnimation()
                        self.answerQuestion1(filmsArray: allFilms)
                        self.answerQuestion2(filmsArray: allFilms)
                        self.answerQuestion3(filmsArray: allFilms)
                        self.answer4Label.text = "--"
                    }
                    else {
                        self.filmsUrl = nextPage!
                        self.getQuestionAnswersAction(sender)
                    }
                }
                else {
                    self.showAlert(message: "Connection error")
                    self.selectButtons(isSelected: false)
                }
            }
        }
        else {
            resetValues()
        }
    }
    
    
    @IBAction func heighlightAction(_ sender: Any) {
        self.selectButtons(isSelected: !actionButton.isSelected)
    }
    
    // select buttons
    func selectButtons(isSelected : Bool) {
        leftStarButton.isSelected = isSelected
        rightStartButton.isSelected = isSelected
        actionButton.isSelected = isSelected
    }
    
    
    // reset answers
    func resetValues() {
        answer1Label.text = ""
        answer2Label.text = ""
        answer3Label.text = ""
        answer4Label.text = ""
        stopStarWarAnimation()
    }
    
    func answerQuestion1 (filmsArray : [Film]) {
        let answer1 = Business.getLongestOpeningCrawl(filmsArray: filmsArray)
        answer1Label.text = answer1
    }
    
    func answerQuestion2 (filmsArray : [Film]) {
        let charachterUrl = Business.getMostAppearCharchter(filmsArray: filmsArray)
        answer2loading.startAnimating()
        
        // get character data
        
        Services.getObject(url: charachterUrl) { (error, object) in
            self.answer2loading.stopAnimating()
            let character = Character(jsonObject: object!)
            self.answer2Label.text = character.name
        }
    }
    
    
    func answerQuestion3 (filmsArray : [Film]) {
        answer3loading.startAnimating()
        let result = Business.getMostAppearSpecies(filmsArray: filmsArray)
        let listOfSpeciesUrls = result.listOfSpeciesUrls
        let numberOfrepeate = result.numberOfrepeate
        var resultString = ""
        for url in listOfSpeciesUrls {
            Services.getObject(url: url) { (error, object) in
                self.answer2loading.stopAnimating()
                let specie = Specie(jsonObject: object!)
                
                if(resultString == "") {
                    resultString = "\(specie.name) (\(numberOfrepeate))"
                }
                else {
                    resultString = "\(resultString) \n \(specie.name) (\(numberOfrepeate))"
                }
                self.answer3Label.text = resultString
                if(url == listOfSpeciesUrls.last) {
                    self.answer3loading.stopAnimating()
                }
            }
        }
    }
    
    
    // animate bar on star war logo
    
    func startStarWarAnimation (value : CGFloat) {
        if(starWarAnimating == false) {
            return
        }
        DispatchQueue.main.async {
            self.leadingConstantAnimationBar.constant = value
            UIView.animate(withDuration: 0.75) {
                self.view.layoutIfNeeded()
            }
        }
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (Timer) in
            var newValue : CGFloat = 0.0
            if(value == 220) {
                newValue = 0
            }
            else {
                newValue = 220
            }
            self.startStarWarAnimation(value: newValue)
        }
    }
    
    // stop animate bar on star war logo
    
    func stopStarWarAnimation () {
        starWarAnimating = false
        self.leadingConstantAnimationBar.constant = 0
    }
    
}
