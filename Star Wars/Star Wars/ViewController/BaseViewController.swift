//
//  BaseViewController.swift
//  Star Wars
//
//  Created by DXBSS-MACLTP on 10/5/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showAlert (message: String) {
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }

}
