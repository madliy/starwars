//
//  Company.swift
//  DPW
//
//  Created by DXBSS-MACLTP on 6/20/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Services: NSObject {
    
    static let domain = "https://swapi.co/api/"
    static let filmsUrl = domain + "films/"
    
    
    class func getAllFilms(url : String , completion: @escaping (_ error: Error?, _ allFilms: [Film] , _ nextpage : String?)->Void) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    completion(error, [] , nil)
                    print(error)
                    
                case .success(let value):
                    let jsonResult = JSON(value)
                    
                    let allfilmsMethod = Business.getAllFilms(json: jsonResult)
                    
                    completion(nil, allfilmsMethod.allFilms , allfilmsMethod.nextpage)
                }
        }
    }
    
    
    class func getObject(url : String , completion: @escaping (_ error: Error?, _ object: JSON?)->Void) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                    print(error)
                    
                case .success(let value):
                    let jsonResult = JSON(value)
                    completion(nil, jsonResult)
                }
        }
    }
}
