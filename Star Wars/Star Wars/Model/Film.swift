//
//  Film.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Film: NSObject {
    var title = ""
    var opening_crawl = ""
    var episode_id = 0
    var release_date = ""
    var edited = ""
    var created = ""
    var url = ""
    var producer = ""
    var director = ""
    var characters = [String]()
    var species = [String]()
    var starships = [String]()
    var vehicles = [String]()
    var planets = [String]()
    
    init(jsonObject : JSON) {
        title = jsonObject["title"].stringValue
        opening_crawl = jsonObject["title"].stringValue
        episode_id = jsonObject["episode_id"].intValue
        release_date = jsonObject["release_date"].stringValue
        edited = jsonObject["edited"].stringValue
        created = jsonObject["created"].stringValue
        url = jsonObject["url"].stringValue
        producer = jsonObject["producer"].stringValue
        director = jsonObject["director"].stringValue
        
        
        for object in jsonObject["characters"].arrayObject! {
            characters.append(object as! String)
        }
        
        for object in jsonObject["species"].arrayObject! {
            species.append(object as! String)
        }
        
        for object in jsonObject["starships"].arrayObject! {
            starships.append(object as! String)
        }
        
        for object in jsonObject["vehicles"].arrayObject! {
            vehicles.append(object as! String)
        }
        
        for object in jsonObject["planets"].arrayObject! {
            planets.append(object as! String)
        }
        
    }
    
}
