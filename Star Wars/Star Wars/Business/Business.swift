//
//  Business.swift
//  StarWars
//
//  Created by DXBSS-MACLTP on 10/4/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Business: NSObject {
    class func getAllFilms(json : JSON) -> (allFilms : [Film], nextpage : String) {
        var allFilms = [Film]()
        
        let next = json["next"].stringValue
        
        let results = json["results"].array ?? []
        results.forEach { item in allFilms.append(Film(jsonObject: item)) }
        
        return (allFilms , next)
    }
    
    
    class func getLongestOpeningCrawl(filmsArray : [Film]) -> String {
        if let longestOpenCrawelFilm = filmsArray.max(by: {$1.opening_crawl.count > $0.opening_crawl.count}) {
            return longestOpenCrawelFilm.title
        }
        return ""
    }
    
    class func getMostAppearCharchter(filmsArray : [Film]) -> String {
        
        var charachterUrl = ""
        var allCharachters = [String]()
        
        // create array for all characters for all films
        filmsArray.forEach { film in allCharachters.append(contentsOf: film.characters) }
        
        let countedSet = NSCountedSet(array: allCharachters)
        
        // get the max repeated url
        charachterUrl = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        return charachterUrl
    }
    
    
    
    class func getMostAppearSpecies(filmsArray : [Film]) -> (listOfSpeciesUrls : [String] , numberOfrepeate : Int) {
        
        var maxNumberOfRepeated = 0
        
        var allSpecies = [String]()
        
        // create array for all specie for all films
        filmsArray.forEach { film in allSpecies.append(contentsOf: film.species) }
        
        
        var counts = [String: Int]()
        allSpecies.forEach { counts[$0] = (counts[$0] ?? 0) + 1 }
        
        // Find the most frequent value and its count with max(by:)
        if let (result) = counts.max(by: {$0.1 < $1.1}) {
            maxNumberOfRepeated = result.value
        }
        
        // find all specie which have max number of repeat
        let maxItemsDuplicated = Array(Set(allSpecies.filter({ (i: String) in allSpecies.filter({ $0 == i }).count > maxNumberOfRepeated - 1})))
        
        return (maxItemsDuplicated , maxNumberOfRepeated)
    }
}
